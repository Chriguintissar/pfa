const express = require("express");
const app = express();
const fs = require("fs");
var bodyparser=require("body-parser");
const port = 8000;
const jwt = require('jsonwebtoken');
var mongoose =require('mongoose');
var User = require('./models/User');
const path = require("path");
const crypto = require("crypto");
const multer = require("multer");
const GridFsStorage = require("multer-gridfs-storage");
var cacheControl=require('express-cache-controller');
const bcrypt = require('bcrypt');
const saltRounds = 10;


// DB
const mongoURI = "mongodb://localhost/pfa";
// connection
mongoose.connect(mongoURI ,{ useNewUrlParser: true });
const conn = mongoose.connection;

app.listen(port, function(error) {
  if (error) {
    console.log("something wrong", error);
  } else {
    console.log("server is listening on port " + port);
  }
  //upload file to db
  const upload = multer({ storage: storage });

});
// Middlewares
app.use(cacheControl());
app.use(bodyparser.json());
app.use(express.json());
app.set("view engine", "ejs");
// init gfs
let gfs;
conn.once("open", () => {
  // init stream
  gfs = new mongoose.mongo.GridFSBucket(conn.db, {
    bucketName: "uploads"
  });
});

// Storage
const storage = new GridFsStorage({
  url: mongoURI,
  file: (req, file) => {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        
        const filename = buf.toString("hex") + path.extname(file.originalname);
        const fileInfo = {
          filename: filename,
          metadata: {originalName: file.originalname},
          bucketName: "uploads"
        };
        resolve(fileInfo);
      });
    });
  }
});

const upload = multer({
  storage
});

app.post("/upload", upload.single("file"), (req, res) => {
  //res.json({file : req.file})
  console.log(req.file);
  res.send("ok");        

 });
//get file by filename from db
app.get("/getFile/:filename", (req, res) => {
  // console.log('id', req.params.id)
  const file = gfs
    .find({
      filename: req.params.filename
    })
    .toArray((err, files) => {
      if (!files || files.length === 0) {
        return res.status(404).json({
          err: "no files exist"
        });
      }
      gfs.openDownloadStreamByName(req.params.filename).pipe(res);
    });
});
//get files
app.get("/files", (req, res) => {
  gfs.find().toArray((err, files) => {
    // check if files
    if (!files || files.length === 0) {
      return res.status(404).json({
        err: "no files exist"
      });
    }

    return res.json(files);
  });
});
// Delete files from the db
app.post("/files/del/:id", (req, res) => {
  gfs.delete(new mongoose.Types.ObjectId(req.params.id), (err, data) => {
    if (err) return res.status(404).json({ err: err.message });
    res.send("deleted successfully");
  });
});


 //get token from url
app.get('/api/streaming', (req, res) => {  
  console.log(__dirname);
  var token = req.query.token;
  jwt.verify(token, 'secretkey', (err, authData) => {
    if(err) {
      res.sendStatus(403);
    } else {
      try {
        if (fs.existsSync(req.query.path)) {
          //file exists
          console.log("file exist");
          res.set('Cache-Control', 'public, max-age=31557600');
          res.writeHead(200, { "content-type": "video/mp4" });
          res.cacheControl = {
            maxAge: 3000
        };
          var rs = fs.createReadStream(req.query.path);
          rs.pipe(res);
        } else {
          console.log("file doesn't exist");
          res.send({ succes: true, message: "file doesn't exist" });
        }
      } catch (err) {
        console.error(err);
      }
    
    }
  });
});


 //get token from header
app.get('/api/download', verifyToken, (req, res) => {  
  jwt.verify(req.token, 'secretkey', (err, authData) => {
    if(err) {
      res.sendStatus(403);
    } else {
      try {
        if (fs.existsSync(req.query.path)) {
          //file exists
          console.log("file exist");
          //res.send({ succes: true, message: "file exist" });
          res.writeHead(200, { "content-type": "video/mp4" });
          var rs = fs.createReadStream(req.query.path);
          rs.pipe(res);
        } else {
          console.log("file doesn't exist");
          res.send({ succes: true, message: "file doesn't exist" });
        }
      } catch (err) {
        console.error(err);
      }
    
    }
  });
});

//register
app.post('/api/register', (req, res) => {
  
  var username=req.body.username;
  var email=req.body.email;
  var password=bcrypt.hashSync(req.body.password, saltRounds);
  user =  User.findOne({ email:email },function(err,user){
  
    if(user){
     res.json({
       "code": "500","msg":"email existe déja"
     });
    }else{
  var user=new User();
  user.username=username;
  user.email=email;
  user.password=password;
  user.save((err,result)=>{
 
    if(err){
      console.log("there is an error in adding user in db")
    res.sendStatus(500);
     }
     res.sendStatus(200);
  })
 }
 })
});

//login
app.post('/api/login', (req, res) => {
  var password=req.body.password;
  var email=req.body.email;
   user =  User.findOne({ email:email },function(err,user){

    
     if(err){
       console.log(err)
        res.sendStatus(500);
     }
     if(user){
      if(bcrypt.compareSync(password, user.password)) {
        jwt.sign({user}, 'secretkey', (err, token) => {
          res.json({
            "code": "200","msg":"login succ",
            token
          });
        });
         
      }else{
         res.json({"code": "204","msg":"password incorrect"});
      }
    }else{
      res.json({"code": "204","msg":"email incorrect"});
    }
    
   });
 
});

// FORMAT OF TOKEN
// Authorization: Bearer <access_token>

// Verify Token
function verifyToken(req, res, next) {
  // Get auth header value
  const bearerHeader = req.headers['authorization'];
  // Check if bearer is undefined
  if(typeof bearerHeader !== 'undefined') {
    // Split at the space
    const bearer = bearerHeader.split(' ');
    // Get token from array
    const bearerToken = bearer[1];
    // Set the token
    req.token = bearerToken;
    // Next middleware
    next();
  } else {
    // Forbidden
    res.sendStatus(403);
  }

};